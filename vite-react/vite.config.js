import { defineConfig } from "vite"
import react from "@vitejs/plugin-react"
import requireTransform from 'vite-plugin-require-transform';
// import reactRefresh from '@vitejs/plugin-react-refresh';
import commonjs from 'vite-plugin-commonjs';
import path from "path"
import { fileURLToPath } from "url"

const __dirname = path.dirname(fileURLToPath(import.meta.url))
const alias = [
  { find: "@", replacement: path.resolve(__dirname, "src") },
  { find: "components", replacement: path.resolve(__dirname, "src/components") },
  { find: "config", replacement: path.resolve(__dirname, "src/config") },
  { find: "store", replacement: path.resolve(__dirname, "src/store") },
  { find: "utils", replacement: path.resolve(__dirname, "src/utils") },
  { find: /^~/, replacement: "" }
]

// https://vitejs.dev/config/

export default defineConfig({
  plugins: [
    // reactRefresh(),
    react(),
    requireTransform({
      fileRegex: /.js$|.jsx$ | .vue$/,
    }),
    commonjs()
  ],
  resolve: {
    alias
  }
})
