import { configureStore } from "@reduxjs/toolkit"

import cart from "./carts/cart"

const store = configureStore({
  reducer: cart.reducer
})

export default store
