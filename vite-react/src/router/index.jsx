/*
 * @Author: liuzisheng 1171764830@qq.com
 * @Date: 2023-11-14 11:07:03
 * @LastEditors: liuzisheng 1171764830@qq.com
 * @LastEditTime: 2023-11-15 13:18:14
 * @FilePath: /meeting/vite-react/src/router/index.jsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Classroom from "@/pages/classroom/index"
import React from "react"
import Conference from "@/pages/conference/index"
// import Login from "@/pages/login/index"
import Register from "@/pages/register/index"
// import NotFound from "@/pages/notFound/index"

const Login = require("@/pages/login").default

//  ‘/’ 下的路由
import NotFound from "@/pages/notFound/index"
import Proview from "@/pages/conference/proview"
//  ‘/’ 下的路由W
const staticRouter = [
  {
    path: "/login",
    name: "登录",
    icon: "",
    element: <Login />
  },
  {
    path: "/register",
    name: "注册",
    icon: "",
    element: <Register />
  },
  {
    path: "/proview",
    name: "详情页",
    icon: "",
    element: <Proview />
  },
  {
    path: "/conference",
    name: "教室主页",
    icon: "",
    element: <Conference />
  },
  {
    path: "/admin/conference/create",
    name: "会议申请",
    icon: "",
    element: <Conferencecreate />
  }
  // {
  //   path: "/404",
  //   name: "404",
  //   icon: "",
  //   element: <NotFound />
  // }
]
const adminRouter = [
  {
    path: "/admin",
    name: "首页",
    icon: "",
    children: [
      {
        path: "/admin/classroom",
        name: "教室模块",
        icon: "",
        element: <Classroom />
      },
      {
        path: "/admin/conference",
        name: "会议模块",
        icon: "",
        element: <Conference />
      }
    ]
  },
  {
    path: "/404",
    name: "404",
    icon: "",
    element: <NotFound />
  }
]

export { staticRouter, adminRouter }
