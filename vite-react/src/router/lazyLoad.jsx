import { lazy, Suspense } from "react"
const LazyLoad = (props) => {
  // 配置动态加载组件路径
  const Element = props && lazy(() => import.meta.glob("@/page/**/**.jsx"))
  return (
    // 返回组件，Suspense:懒加载组件
    <Suspense fallback={<h3>加载中. . . </h3>}>
      {/* 内容组件 */}
      <Element />
    </Suspense>
  )
}

export default LazyLoad
