import axios from "axios"

//判断是否是生产环境
const isMode = import.meta.env.MODE === "development"
// const token = JSON.parse(sessionStorage.getItem("token"))
let baseURL = isMode ? "http://localhost:3666" : "http://localhost:3666"

// 添加请求拦截器
axios.interceptors.request.use(
  (config) => {
    // 在发送请求之前做些什么
    const token = JSON.parse(sessionStorage.getItem("token"))
    if (token) {
      config.headers.Authorization = token
    }
    return config
  },
  (error) => {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// // 在请求发送前检查Token的有效性
// axios.interceptors.request.use(
//   async (config) => {
//     const accessToken = JSON.parse(sessionStorage.getItem("token"))
//     if (accessToken) {
//       config.headers.Authorization = accessToken
//     }
//     return config
//   },
//   (error) => {
//     return Promise.reject(error)
//   }
// )

// // 在响应接收后检查Token是否过期，并进行刷新
// axios.interceptors.response.use(
//   (response) => {
//     return response
//   },
//   async (error) => {
//     const originalRequest = error.config
//     if (error.response.status === 401 && !originalRequest._retry) {
//       originalRequest._retry = true
//       try {
//         const refreshToken = JSON.parse(sessionStorage.getItem("refreshToken"))
//         const response = await axios.post("/auth/refresh", { refreshToken })
//         const newAccessToken = response.data.accessToken
//         sessionStorage.setItem("token", JSON.stringify(newAccessToken))
//         originalRequest.headers.Authorization = newAccessToken
//         return axios(originalRequest)
//       } catch (error) {
//         // 处理无法刷新Token的情况
//       }
//     }
//     return Promise.reject(error)
//   }
// )

const request = ({ url, path = "", params = {}, data, method = "POST", ...rest }) => {
  return new Promise((resole, reject) => {
    return axios({
      method,
      url: url || baseURL + path,
      data: {
        ...data,
        ...params
      },
      ...rest
    })
      .then((res) => {
        resole(res && res.data)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
export default request
