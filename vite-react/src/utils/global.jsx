// 存token
const setToken = (key, token) => {
  return window.localStorage.setItem(key, token)
}
// 取token
const getToken = (key) => {
  return window.localStorage.getItem(key)
}
// 删token
const removeToken = (key) => {
  return window.localStorage.removeItem(key)
}

export { setToken, getToken, removeToken }
