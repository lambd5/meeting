import React, { useEffect, useState } from "react"
import { Descriptions, Divider, Button } from "antd"
import { conferenceDetails } from "@/api/modules/conference/index.jsx"
import { useNavigate } from "react-router-dom"
import { ArrowLeftOutlined } from "@ant-design/icons"
import "./style.less"

const Proview = () => {
  //数据
  const [items, setitems] = useState([])
  // 路由跳转
  const navigate = new useNavigate()

  //数据获取
  const getdata = async () => {
    let paramslist = window.location.search.slice(1).split("=")[1]
    let { data } = await conferenceDetails({ _id: paramslist })
    // 赋值
    setitems([
      {
        key: "1",
        label: "会议名称",
        children: data[0].conference_name
      },
      {
        key: "2",
        label: "会议主题",
        children: data[0].conference_theme
      },
      {
        key: "3",
        label: "使用类型",
        children: data[0].use_type ? data[0].use_type.type_name : "未获取到使用信息"
      },
      {
        key: "4",
        label: "使用时间",
        children: data[0].use_start_time.slice(0, 10) + "至" + data[0].use_end_time.slice(0, 10)
      },
      {
        key: "5",
        label: "申请人",
        children: data[0].apply_name
      },
      {
        key: "6",
        label: "申请部门",
        children: data[0].apply_department
          ? data[0].apply_department.deparment_name
          : "未获取到部门信息"
      }
    ])
  }

  const goback = () => {
    navigate(-1)
  }
  // 有接口则放开
  useEffect(() => {
    getdata()
  }, [])
  return (
    <div className="preview">
      <Descriptions title="会议详情" items={items} />
      <Divider orientation="left">收款信息</Divider>
      <p>暂无</p>
      <Divider orientation="left">退款信息</Divider>
      <p>暂无</p>
      <div className="gouback">
        <Button onClick={() => goback()} className="goback">
          <ArrowLeftOutlined />
          返回
        </Button>
      </div>
    </div>
  )
}

export default Proview
