import { useState, useEffect } from "react"
import { Closnum } from "./tableClo"
import { Card, Form, Input, Button, Row, Col, Table } from "antd"
import { useForm } from "antd/lib/form/Form"
import { SearchOutlined } from "@ant-design/icons"
import axios from "axios"
// import moment from "moment"
import dayjs from "dayjs"

const Application = () => {
  const [form] = useForm()
  const [list, setList] = useState([])
  // const [dates, setDates] = useState(null)
  // const [value, setValue] = useState(null)
  const getList = () => {
    axios.get("http://localhost:3000/classList").then((res) => {
      setList(res.data)
    })
  }
  useEffect(() => {
    // getList()
  }, [])

  // const getDay = () => {
  //   return moment().startOf("day")
  // }

  // const getDatS = (day, num) => {
  //   return moment(day).add(num, "days")
  // }

  const onFinish = (value) => {
    const useStartTime = dayjs(value.time[0]).format("YYYY年MM月DD日 HH:mm")
    const useEndTime = dayjs(value.time[1]).format("YYYY年MM月DD日 HH:mm")
    value.use_start_time = useStartTime
    value.use_end_time = useEndTime
  }

  // const onOpenChange = (open) => {
  //   open ? setDates([null, null]) : setDates(null)
  // }

  // const disabledDate = (current) => {
  //   if (!dates) {
  //     return false
  //   }
  //   if (dates[0] === null || current < getDay()) {
  //     // 计算7天后的日期
  //     const sevenDays = getDatS(getDay(), 7)
  //     return current && (current < getDay() || current > sevenDays)
  //   } else {
  //     const tooLate = dates[0] && current.diff(dates[0], "days") >= 7
  //     const tooEarly = dates[1] && dates[1].diff(current, "days") >= 7
  //     return !!tooEarly || !!tooLate
  //   }
  // }
  return <div>{/* <Table columns={Closnum} dataSource={list} /> */}</div>
}

export default Application
