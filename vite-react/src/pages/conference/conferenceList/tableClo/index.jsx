import { useEffect, useRef, useState } from "react"
import { Button, Popconfirm, Table } from "antd"
// import "./style.less"
import { useNavigate } from "react-router-dom"
import { Booklist, conferenceExamine } from "@/api/modules/conference/index.jsx"

const Tablemodel = (props) => {
  // 路由跳转
  const navigate = new useNavigate()

  //分页信息
  // setPaginationProps
  const [paginationProps, setpaginationProps] = useState({
    pageSize: 5, // 每页数据条数
    // total: 0, // 总条数
    total: props.data_length, // 总条数
    //改变页码的函数
    onChange: (page, pageSize) => handlePageChange(page, pageSize),
    // hideOnSinglePage: true,
    showSizeChanger: true,
    pageSizeOptions: [5, 10, 20, 50, 100],
    position: "bottomRight"
    // onShowSizeChange: (current, size) => onSizeChange(current, size)
  })

  // 更变页码数回调
  const handlePageChange = (page, pageSize) => {
    let copypaginationProps = paginationProps
    copypaginationProps.pageSize = pageSize
    setpaginationProps(copypaginationProps)
  }

  //列表项
  const columns = [
    {
      title: "使用时间",
      dataIndex: "use_start_end_time",
      key: "use_start_end_time",
      width: 140,
      render: (_, { use_start_time, use_end_time }) => (
        <>
          <span>{use_start_time.slice(0, 16)}至</span>
          <br />
          <span>{use_end_time.slice(0, 16)}</span>
        </>
      )
    },
    {
      title: "申请人",
      dataIndex: "apply_name",
      key: "apply_name",
      width: 90,
      render: (_, { apply_name }) => <span>{apply_name}</span>
    },
    {
      title: "申请部门",
      dataIndex: "apply_department",
      key: "apply_department",
      width: 100,
      render: (_, { apply_department }) => (
        // <span>{apply_department.deparment_name}</span>
        // <span>未知A</span>
        <span>
          {apply_department == undefined ? "未获取到使用信息" : apply_department.deparment_name}
        </span>
      )
    },
    {
      title: "会议主题",
      dataIndex: "conference_name",
      key: "conference_name",
      width: 90
    }
  ]

  //调用删除接口
  const conference_del = async (_id) => {
    await conference_del(_id)
  }

  //日期格式化
  function pad(num) {
    return num.toString().padStart(2, "0")
  }
  const remigrate = (date_untreated) => {
    let date = new Date(date_untreated)
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
    return `${year}-${pad(month)}-${pad(day)} ${pad(hour)}:${pad(minute)}:${pad(second)}`
  }
  // 数据
  const [Rooms, setRooms] = useState(props)
  // console.log(props, "筛选教师")
  // const Bookroom = BookList(props)
  const [bookList, setbookList] = useState([])
  const BookList = async (Rooms) => {
    let lista = await Booklist(Rooms)
    setbookList(lista.data)
    localStorage.setItem("bookDay", JSON.stringify(lista.Daysa) || [])
    // if (lista.Daysa) {
    //   localStorage.setItem("bookDay", "[]")
    // } else {
    //   localStorage.setItem("bookDay", JSON.stringify(lista.Daysa) || [])
    // }

    console.log(lista, "123预约数据")
  }
  // BookList(props)
  //页面渲染前获取数据，权限
  useEffect(() => {
    // get_list()
    BookList(props)
  }, [props])

  // scroll={{ y: 140 }}
  return (
    <div className="reservation-table">
      <Table
        scroll={{ y: 240 }}
        columns={columns}
        dataSource={bookList}
        pagination={{
          total: bookList.length, // 总条数
          showSizeChanger: true,
          pageSizeOptions: [3, 5, 10, 20, 50],
          position: "bottomLeft"
        }}
      />
      {/* <Table scroll={{ y: 140 }} columns={columns} dataSource={dt} pagination={paginationProps} /> */}
    </div>
  )
}

export default Tablemodel
