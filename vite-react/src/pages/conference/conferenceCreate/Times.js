function Times(starTime) {
  let showTime =
    starTime.getFullYear() +
    "-" +
    (starTime.getMonth() + 1) +
    "-" +
    starTime.getDate() +
    " " +
    starTime.getHours() +
    ":" +
    starTime.getMinutes() +
    ":" +
    starTime.getSeconds()

  return showTime
}

export default Times
