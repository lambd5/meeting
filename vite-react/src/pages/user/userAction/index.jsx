import { useState, useEffect } from "react"
import { userAdd, userEdit, roleList, userList } from "@/api/index.jsx"
import { Button, Modal, Form, Col, Input, Select, Space } from "antd"
const UserAction = (props) => {
  const { isActionOpen, onActionOk, type, _id, userColumns } = props
  const title = (type === "add")
  const typeApi = title ? userAdd : userEdit
  const [form] = Form.useForm()
  const { Option } = Select
  const [loading, setLoading] = useState(false)
  const [open, setOpen] = useState(false)
  useEffect(() => {
    setOpen(isActionOpen)
    isActionOpen && getUserDate({ _id })
  }, [isActionOpen])

  const getUserDate = async (value) => {
    console.log(value)
    const { pageData } = await userList(value)
    !title && form.setFieldsValue({ ...pageData[0] })
  }


  const handleOk = async (value) => {
    title && await typeApi(value)
    !title && await typeApi({ _id, ...value, update_time: Date.now() })

    setLoading(true)
    setTimeout(() => {
      setLoading(false)
      setOpen(!open)
      getUserDate()
    }, 1000)
  }
  const handleCancel = () => {
    onActionOk()
    form.resetFields()
  }
  const getChildren = () => {
    const children = userColumns.map((item, index) => {
      return (
        <Col span={20} key={index}>
          {item.type === "input" && (
            <Form.Item name={item.name} label={item.label} rules={item.rules}>
              <Input placeholder={item.userInfo.placeholder} {...item.userInfo} />
            </Form.Item>
          )}
          {item.type === "input.password" && (
            <Form.Item name={item.name} label={item.label} rules={item.rules}>
              <Input.Password placeholder={item.userInfo.placeholder} {...item.userInfo} />
            </Form.Item>
          )}
          {item.type === "select" && (
            <Form.Item name={item.name} label={item.label} rules={item.rules}>
              <Select placeholder={item.userInfo.placeholder} {...item.userInfo}>
                {item.userInfo.options.map((option) => {
                  return (
                    <Option key={option.title} value={option.value}>
                      {option.title}
                    </Option>
                  )
                })}
              </Select>
            </Form.Item>
          )}
        </Col>
      )
    })
    return [...children]
  }
  return (
    <>
      <Modal
        width={600}
        open={open}
        title={title ? " " : "编辑"}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={false}
      >
        <Form
          form={form}
          name="user-action-form"
          layout="vertical"
          onFinish={handleOk}
          style={{
            marginTop: "20px"
          }}
        >
          <Col gutter={12}>{getChildren()}</Col>
          <Form.Item
            style={{
              marginTop: "20px",
              textAlign: "right"
            }}
          >
            <Space size={"middle"}>
              {type !== "detail" && (
                <Button type="primary" htmlType="submit" loading={loading}>
                  提交
                </Button>
              )}

              <Button htmlType="button" onClick={handleCancel}>
                返回
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}
export default UserAction
