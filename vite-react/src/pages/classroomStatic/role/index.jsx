import { useState } from "react"
import { Button, Form, Select, Input, Col, Row, Space, theme } from "antd"
import { DownOutlined } from "@ant-design/icons"
const RoleSearch = ({ columns, onSearch }) => {
  const { token } = theme.useToken()
  const [form] = Form.useForm()
  const [expand, setExpand] = useState(false)

  const formStyle = {
    maxWidth: "none",
    background: token.colorFillAlter,
    borderRadius: token.borderRadiusLG,
    padding: 24
  }

  const getFields = () => {
    const total = columns.length
    const count = expand ? total : 4
    let list = columns.slice(0, count)
    const children = list.map((item, index) => {
      return (
        <Col span={6} key={index}>
          {item.type === "input" && (
            <Form.Item name={item.name} label={item.label} {...item}>
              <Input placeholder={item.placeholder} />
            </Form.Item>
          )}
          {item.type === "select" && (
            <Form.Item name={item.name} label={item.label}>
              <Select
                placeholder={item.placeholder}
                fieldNames={item.fieldNames && item.fieldNames ? item.fieldNames : ""}
                options={item.options}
              >
                {/* {item.options.map((option) => {
                  return (
                    <Option value={option.value} key={option.value}>
                      {option.title}
                    </Option>
                  )
                })} */}
              </Select>
            </Form.Item>
          )}
        </Col>
      )
    })
    return [...children]
  }
  const onFinish = (values) => {
    onSearch(values)
  }
  return (
    <Form form={form} name="advanced_search" style={formStyle} onFinish={onFinish}>
      <Row gutter={6}>{getFields()}</Row>
      <div
        style={{
          textAlign: "right"
        }}
      >
        <Space size="small">
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
          <Button
            onClick={() => {
              onSearch({})
              form.resetFields()
            }}
          >
            重置
          </Button>

          {columns.length > 4 && (
            <a
              style={{
                fontSize: 12
              }}
              onClick={() => {
                setExpand(!expand)
              }}
            >
              <DownOutlined rotate={expand ? 180 : 0} />
              {expand ? "收起" : "展开"}
            </a>
          )}
        </Space>
      </div>
    </Form>
  )
}

export default RoleSearch
