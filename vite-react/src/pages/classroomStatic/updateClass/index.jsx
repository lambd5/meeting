import { useEffect, useState } from "react"
import { Button, Card, Form, Input, Select } from "antd"
import { useLocation, useNavigate } from "react-router-dom"
import { getClass, getType, getDropt, changeClass } from "@/api/index"
import { useForm } from "antd/lib/form/Form"
import "./index.less"
export default function UpdateClass() {
  const navigate = useNavigate()
  const location = useLocation()
  const searchParams = new URLSearchParams(location.search)
  const id = searchParams.get("id")
  let timer = null
  const [loading, setLoading] = useState(false)
  const [arr, setArr] = useState({})
  const rules = [
    {
      required: true,
      message: "该字段不能为空"
    }
  ]
  const [form] = useForm()
  const [useType, setType] = useState([])
  const [dropt, setDropt] = useState([])
  const applyType = [
    {
      label: "可使用",
      value: 0
    },
    {
      label: "使用中",
      value: 1
    }
  ]
  useEffect(() => {
    getList()
    return () => clearTimeout(timer)
  }, [id])
  useEffect(() => {
    getDeparmentList()
    getTypes()
  }, [])
  const getList = async () => {
    const { data } = await getClass({ _id: id })
    let arr = data[0]
    setArr(arr)
    let deparmentName = arr.apply_department._id
    let typeName = arr.availabel_types._id
    let currentState = arr.current_state
    form.setFieldsValue({
      _id: id,
      current_state: currentState,
      apply_department: deparmentName,
      availabel_types: typeName,
      class_name: arr.class_name
    })
  }
  const getDeparmentList = async () => {
    const { data } = await getType()
    setDropt(data)
  }
  const getTypes = async () => {
    const { data } = await getDropt()
    setType(data)
  }
  const onFinish = async (value) => {
    setLoading(true)
    let obj = Object.fromEntries(Object.entries(value).filter((item) => item[1] !== undefined))
    console.log(obj.current_state != arr.current_state)
    console.log(typeof obj.current_state)
    console.log(typeof arr.current_state)
    obj.current_state != arr.current_state ? (obj.update_time = Date.now() * 1) : false
    await changeClass({ ...obj })
    timer = setTimeout(() => {
      setLoading(false)
      navigate(-1)
    }, 1000)
  }
  return (
    <Card style={{ width: "100%", border: "none", height: "100%" }}>
      <div className="role-list-title">
        <h3>修改教室</h3>
      </div>
      <Form form={form} onFinish={onFinish} className="form">
        <Form.Item name="_id">
          <Input placeholder="请输入教室名称" type="hidden" />
        </Form.Item>
        <Form.Item label="教室名称" name="class_name" rules={rules}>
          <Input placeholder="请输入教室名称" />
        </Form.Item>
        <Form.Item label="所属院校" name="apply_department" rules={rules}>
          <Select
            placeholder="请选择所属院校"
            fieldNames={{
              label: "deparment_name",
              value: "_id"
            }}
            options={useType}
          />
        </Form.Item>
        <Form.Item label="可用状态" name="current_state" rules={rules}>
          <Select placeholder="请选择可用状态" options={applyType} />
        </Form.Item>
        <Form.Item label="使用类型" name="availabel_types" rules={rules}>
          <Select
            placeholder="请选择使用类型"
            fieldNames={{
              label: "type_name",
              value: "_id"
            }}
            options={dropt}
          />
        </Form.Item>
        <div style={{ float: "bottom" }} className="btn">
          <Form.Item style={{ float: "bottom" }}>
            <Button type="primary" htmlType="submit" loading={loading} className="button">
              修改
            </Button>
            <Button
              className="button"
              onClick={() => {
                //表单清空
                form.resetFields()
              }}
            >
              重置
            </Button>
            <Button
              className="button"
              onClick={() => {
                navigate(-1)
              }}
            >
              返回
            </Button>
          </Form.Item>
        </div>
      </Form>
    </Card>
  )
}
