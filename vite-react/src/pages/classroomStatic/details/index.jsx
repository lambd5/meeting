import { useEffect, useState } from "react"
import { Button, Descriptions } from "antd"
import { useLocation, useNavigate } from "react-router-dom"
import { ArrowLeftOutlined } from "@ant-design/icons"
import { getClass } from "@/api/index"
export default function Details() {
  const navigate = useNavigate()
  const location = useLocation()
  const searchParams = new URLSearchParams(location.search)
  const id = searchParams.get("id")
  const [items, setItems] = useState([])
  useEffect(() => {
    getList()
  }, [id])
  const getList = async () => {
    const { data } = await getClass({ _id: id })
    let arr = data[0]
    let deparmentName = arr.apply_department.deparment_name
    let typeName = arr.availabel_types.type_name
    setItems([
      {
        key: "1",
        label: "会议名称",
        children: arr.class_name
      },
      {
        key: "2",
        label: "所属学院",
        children: deparmentName
      },
      {
        key: "3",
        label: "可用状态",
        children: arr.current_state === 0 ? "可用" : "不可用"
      },
      {
        key: "4",
        label: "使用类型",
        children: typeName
      }
    ])
  }
  const goBack = () => {
    navigate(-1)
  }
  return (
    <div>
      <Descriptions
        title={
          <>
            <Button type="text" onClick={() => goBack()}>
              <ArrowLeftOutlined />
              返回
            </Button>
            <b>教室详情</b>
          </>
        }
        items={items}
      />
    </div>
  )
}
