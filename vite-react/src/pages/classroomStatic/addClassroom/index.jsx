import { useEffect, useState } from "react"
import { Form, Input, Button, Select, Card } from "antd"
import { useForm } from "antd/lib/form/Form"
import { useNavigate } from "react-router-dom"
import { getType, getDropt, addClass } from "@/api/index"
import "./index.less"
export default function AddClassroom() {
  const navigate = useNavigate()
  let timer = null
  const [loading, setLoading] = useState(false)
  const rules = [
    {
      required: true,
      message: "该字段不能为空"
    }
  ]
  const [form] = useForm()
  const [useType, setType] = useState([])
  const [dropt, setDropt] = useState([])
  const applyType = [
    {
      label: "可使用",
      value: 0
    },
    {
      label: "使用中",
      value: 1
    }
  ]
  useEffect(() => {
    getDeparmentList()
    getTypes()
    return () => clearTimeout(timer)
  }, [])
  const getDeparmentList = async () => {
    const { data } = await getType()
    setDropt(data)
  }
  const getTypes = async () => {
    const { data } = await getDropt()
    setType(data)
  }
  const onFinish = async (value) => {
    setLoading(true)
    value.create_time = Date.now()
    await addClass({ ...value })
    timer = setTimeout(() => {
      setLoading(false)
      navigate(-1)
    }, 1000)
  }
  return (
    <Card style={{ width: "100%", border: "none", height: "100%" }}>
      <div className="role-list-title">
        <h3>添加教室</h3>
      </div>
      <Form form={form} onFinish={onFinish} style={{ height: "100%" }}>
        <Form.Item label="教室名称" name="class_name" rules={rules}>
          <Input placeholder="请输入教室名称" />
        </Form.Item>
        <Form.Item label="所属院校" name="apply_department" rules={rules}>
          <Select
            placeholder="请选择所属院校"
            fieldNames={{
              label: "deparment_name",
              value: "_id"
            }}
            options={useType}
          />
        </Form.Item>
        <Form.Item label="可用状态" name="current_state" rules={rules}>
          <Select placeholder="请选择可用状态" options={applyType} />
        </Form.Item>
        <Form.Item label="使用类型" name="availabel_types" rules={rules}>
          <Select
            placeholder="请选择使用类型"
            fieldNames={{
              label: "type_name",
              value: "_id"
            }}
            options={dropt}
          />
        </Form.Item>
        <div style={{ float: "bottom" }} className="btn">
          <Form.Item style={{ float: "bottom" }}>
            <Button type="primary" htmlType="submit" loading={loading} className="button">
              添加
            </Button>
            <Button
              className="button"
              onClick={() => {
                //表单清空
                form.resetFields()
              }}
            >
              重置
            </Button>
            <Button
              className="button"
              onClick={() => {
                navigate(-1)
              }}
            >
              返回
            </Button>
          </Form.Item>
        </div>
      </Form>
    </Card>
  )
}
