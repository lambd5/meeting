import { Input, Checkbox, DatePicker, Radio, Select, Form } from "antd"
const { TextArea } = Input
const FormSelect = ({ items }) => {
  if (items.type == "select") {
    return (
      <Form.Item
        style={{ width: 200 }}
        name={items.name}
        label={items.label}
        className={items.className}
      >
        <Select></Select>
      </Form.Item>
    )
  } else if (items.type == "radio") {
    return (
      <Form.Item name={items.name} label={items.label} className={items.className}>
        {" "}
        <Radio> </Radio>
      </Form.Item>
    )
  } else if (items.type == "checkbox") {
    return (
      <Form.Item name={items.name} label={items.label} className={items.className}>
        <Checkbox></Checkbox>
      </Form.Item>
    )
  } else if (items.type == "date") {
    return (
      <Form.Item name={items.name} label={items.label} className={items.className}>
        <DatePicker></DatePicker>
      </Form.Item>
    )
  } else if (items.type == "textare") {
    return (
      <Form.Item
        autoSize
        style={{ width: 200 }}
        name={items.name}
        label={items.label}
        className={items.className}
      >
        <TextArea></TextArea>
      </Form.Item>
    )
  } else if (items.type == "text") {
    return (
      <Form.Item
        style={{ width: 200 }}
        name={items.name}
        label={items.label}
        className={items.className}
      >
        <Input></Input>
      </Form.Item>
    )
  } else if (items.type == "password") {
    return (
      <Form.Item
        style={{ width: 200 }}
        name={items.name}
        label={items.label}
        className={items.className}
      >
        <Input.Password></Input.Password>
      </Form.Item>
    )
  }
}
export default FormSelect
