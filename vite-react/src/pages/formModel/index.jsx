import { useEffect, useState } from "react"
import { Form } from "antd"
import axios from "axios"
import FormSelect from "./form/index"
const FormModel = () => {
  const [formList, setFrom] = useState([])
  const getList = () => {
    axios.get("http://localhost:3000/users").then((req) => {
      setFrom(req.data)
    })
  }
  useEffect(() => {
    getList()
  }, [])
  return (
    <div>
      <Form>
        {formList &&
          formList.map((item) => {
            return <FormSelect key={item.key} items={item} />
          })}{" "}
      </Form>
    </div>
  )
}

export default FormModel
