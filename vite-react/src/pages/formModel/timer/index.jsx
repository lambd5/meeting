import { useEffect, useState } from "react"
import { DatePicker } from "antd"

const Timer = () => {
  const [value, setValue] = useState("year")
  const [disTime, setTime] = useState(false)
  useEffect(() => {}, [])

  const onChange = (date, dateString) => {}
  return (
    <div>
      <DatePicker
        onChange={onChange}
        picker={value}
        showTime={disTime}
        format="YYYY-MM-DD HH:mm:ss"
      />
    </div>
  )
}

export default Timer
