import { Input, Form, Button, Select, Divider, Row, Col } from "antd"

const Application = () => {
  // 表单校验
  const onFinish = (values) => {
    console.log("Success:", values)
  }
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo)
  }
  return (
    <div>
      <h1>会议室申请</h1>
      <div>
        <Form
          name="basic"
          labelCol={{
            span: 5
          }}
          wrapperCol={{
            span: 16
          }}
          style={{
            maxWidth: "100%"
          }}
          initialValues={{
            remember: true
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          {/* Select */}
          <Form.Item
            wrapperCol={{
              span: 6
            }}
            label="会议室名称"
            name="Select"
            rules={[
              {
                required: true,
                message: "请选择会议室"
              }
            ]}
          >
            <Select>
              <Select.Option value="千人报告">千人报告厅</Select.Option>
              <Select.Option value="808">808</Select.Option>
              <Select.Option value="809">809</Select.Option>
            </Select>
          </Form.Item>
          {/* 申请分割线 */}
          <Divider orientation="left">已申请列表</Divider>
          <div>TABLE列表</div>
          <Divider orientation="left"></Divider>
          <Form.Item>
            <Row gutter={8}>
              <Col span={12}>
                <Form.Item
                  wrapperCol={{
                    span: 9
                  }}
                  label="使用类型"
                  name="types"
                  rules={[
                    {
                      required: true,
                      message: "请选择类型"
                    }
                  ]}
                >
                  <Select>
                    <Select.Option value="借用">借用</Select.Option>
                    <Select.Option value="面试">面试</Select.Option>
                    <Select.Option value="会议">会议</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  wrapperCol={{
                    span: 9
                  }}
                  label="主题"
                  name="theme"
                  rules={[
                    {
                      required: true,
                      message: "主题不可空"
                    }
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>
            </Row>
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16
            }}
          >
            <Button type="primary" htmlType="submit">
              确定
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default Application
