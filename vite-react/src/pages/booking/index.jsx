import { Button } from "antd"
import { PlusOutlined } from "@ant-design/icons"
import { useNavigate } from "react-router-dom"
const MeetingToRequest = () => {
  const Navigate = useNavigate()
  return (
    <div>
      <div>
        <Button onClick={() => Navigate("/application")} type="primary" icon={<PlusOutlined />}>
          会议室申请
        </Button>
      </div>
      <h1>预约班级</h1>
    </div>
  )
}

export default MeetingToRequest
