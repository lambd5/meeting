import { Button, Form, Input, message } from "antd"
import { getRegister } from "@/api/index"
import { useNavigate } from "react-router-dom"
import "./style.less"
import { LockOutlined, UsergroupDeleteOutlined } from "@ant-design/icons"

const Register = () => {
  const [form] = Form.useForm()
  const navigate = useNavigate()
  const onFinish = async (values) => {
    if (values.username || values.password) {
      const data = await getRegister(values)
      if (data.code === 200) {
        message.success(data.message)
        navigate("/login")
      } else {
        message.error(data.message)
      }
    } else {
      message.error("请输入用户信息")
    }
  }
  return (
    <div className="register-content">
      <div className="register-c-b">
        <div className="letter">
          <h2>后台管理系统</h2>
          <p>便捷预约，高效管理；随时预约，无忧安排；智能预约，提升效率。</p>
        </div>
        <div className="register-box">
          <h2>用户注册</h2>
          <Form
            form={form}
            name="control-hooks"
            onFinish={onFinish}
            className="lant-form"
            style={{
              maxWidth: 350
            }}
          >
            <Form.Item
              className="lant-form-lab"
              name="username"
              // label="用户名"
              rules={[
                {
                  // required: true,
                  // message: "请输入用户名"
                }
              ]}
            >
              <Input
                prefix={<UsergroupDeleteOutlined />}
                placeholder="请输入用户名"
                className="lant-form-lab-inp"
              />
            </Form.Item>
            <Form.Item
              className="lant-form-lab"
              name="password"
              // label="密码"
              rules={[
                {
                  // required: true,
                  // message: "请输入密码"
                }
              ]}
            >
              <Input.Password
                prefix={<LockOutlined />}
                placeholder="请输入密码"
                className="lant-form-lab-inp"
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="default"
                className="lant-form-btndeng"
                onClick={() => {
                  navigate("/login")
                }}
              >
                去登录
              </Button>
              <Button type="primary" htmlType="submit" className="lant-form-btn">
                注册
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  )
}

export default Register
