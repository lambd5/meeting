import "./style.less"
import { Button, Form, Input, message } from "antd"
import { getLogin, getRouter, getAllRouter } from "@/api/index"
import { useNavigate } from "react-router-dom"
import { useEffect } from "react"
import { LockOutlined, UsergroupDeleteOutlined } from "@ant-design/icons"

const Login = () => {
  const [form] = Form.useForm()
  const navigate = useNavigate()
  useEffect(() => {
    const fetchDynamicRoutes = async () => {
      const data = await getAllRouter()
      if (data.code == 200) {
        sessionStorage.setItem("routers", JSON.stringify(data.data))
      } else {
        message.error(data.message)
      }
    }

    fetchDynamicRoutes()
  }, [])

  // 递归处理
  function buildCategoryTree(data, parentId) {
    let categoryTree = []
    data.forEach((item) => {
      if (item.pid == parentId) {
        item.children = buildCategoryTree(data, item._id)
        categoryTree.push(item)
      }
    })
    return categoryTree
  }

  //存入动态路由
  const handedRouter = async (rid) => {
    const data = await getRouter({ idx: rid })

    if (data.code === 200) {
      const categoryTree = buildCategoryTree(data.data, null)
      localStorage.setItem("role", JSON.stringify(data.data1))
      sessionStorage.setItem("routers", JSON.stringify(categoryTree))
    }
  }

  const onFinish = async (values) => {
    if (values.username || values.password) {
      const data = await getLogin(values)

      if (data.code == 200) {
        sessionStorage.setItem("token", JSON.stringify(data.token))
        sessionStorage.setItem("refreshToken", JSON.stringify(data.refreshToken))
        localStorage.setItem("user", JSON.stringify(data.data))

        await handedRouter(data.data.rid)

        navigate("/admin/conference")
        window.location.reload()
        message.success(data.message)
      } else {
        message.error(data.message)
      }
    } else {
      message.error("请输入用户信息")
    }
  }

  return (
    <div className="login-content">
      <div className="login-c-b">
        <div className="letter">
          <h2>后台管理系统</h2>
          <p>便捷预约，高效管理；随时预约，无忧安排；智能预约，提升效率。</p>
        </div>
        <div className="login-box">
          <h2>用户登录</h2>
          <Form
            form={form}
            name="control-hooks"
            onFinish={onFinish}
            className="lant-form"
            style={{
              maxWidth: 350
            }}
          >
            <Form.Item
              className="lant-form-lab"
              name="username"
              // label="用户名"
              rules={[
                {
                  // required: true,
                  // message: "请输入用户名"
                }
              ]}
            >
              <Input
                placeholder="请输入用户名"
                prefix={<UsergroupDeleteOutlined />}
                className="lant-form-lab-inp"
              />
            </Form.Item>
            <Form.Item
              className="lant-form-lab"
              name="password"
              // label="密码"
              rules={[
                {
                  // required: true,
                  // message: "请输入密码"
                }
              ]}
            >
              <Input.Password
                prefix={<LockOutlined />}
                placeholder="请输入密码"
                className="lant-form-lab-inp"
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" className="lant-form-btn">
                登录
              </Button>
              <Button
                type="default"
                onClick={() => {
                  navigate("/register")
                }}
                className="lant-form-btnzhu"
              >
                立即注册
              </Button>
            </Form.Item>
          </Form>
          {/* <span
          className="ljzhuce"
          onClick={() => {
            navigate("/register")
          }}
        >
          立即注册
        </span> */}
        </div>
      </div>
    </div>
  )
}

export default Login
