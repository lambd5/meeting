import { createContext } from "react"
import { Provider } from "react-redux"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import store from "@/store/index"
import BasicLayout from "@/layout/basicLayout/index"

const dataProviderContext = createContext({})
dataProviderContext.displayName = "DataProviderContext"
const Admin = ({ dataProvider, routes }) => {
  const { list } = routes
  return (
    <Provider store={store}>
      <dataProviderContext.Provider value={dataProvider}>
        <Router>
          <Routes>
            {list.map((item) => {
              if (item.path === "/admin") {
                return (
                  item.children &&
                  item.children.map((res) => (
                    <Route
                      key={res.path}
                      path={res.path}
                      element={<BasicLayout routers={item.children}>{res.element}</BasicLayout>}
                    />
                  ))
                )
              } else {
                return <Route key={item.path} path={item.path} {...item}></Route>
              }
            })}
          </Routes>
        </Router>
      </dataProviderContext.Provider>
    </Provider>
  )
}

export default Admin
