import "./style.less"
import React, { useEffect, useState } from "react"
import { Select, Descriptions, Divider, Input, Button, DatePicker } from "antd"
import { SearchOutlined, PlusOutlined } from "@ant-design/icons"
const Search = () => {
  const handleChange = (value) => {}
  //使用类型列表项
  const [use_type_option, set_use_type_option] = useState([
    {
      value: "1",
      label: "借用"
    },
    {
      value: "2",
      label: "租用"
    },
    {
      value: "3",
      label: "购入"
    }
  ])
  const onChange_use_start_time = (date, dateString) => {
    console.log(date, dateString)
  }
  const onChange_use_end_time = (date, dateString) => {
    console.log(date, dateString)
  }

  return (
    <div className="search">
      <div className="search-li">
        开始时间:
        <DatePicker onChange={onChange_use_start_time} />
        <span>至</span>
        <DatePicker onChange={onChange_use_end_time} />
      </div>
      <div className="search-li">
        会议主题:
        <Input placeholder="" />
      </div>
      <div className="search-li">
        申请人:
        <Input placeholder="" />
      </div>
      <div className="search-li">
        会议室名称:
        <Input placeholder="" />
      </div>
      <div className="search-li">
        使用类型:
        <Select
          defaultValue=""
          style={{
            width: 120
          }}
          onChange={handleChange}
          options={use_type_option}
        />
      </div>
      <div className="search-li">
        {/* 申请状态:
        <Select
            defaultValue={0 }
            style={{
                width: 120,
            }}
            onChange={handleChange}
            options={[
                {
                value: 1,
                label: '预约成功',
                },
                {
                value: 0,
                label: '预约中',
                }
            ]}
        /> */}
      </div>
      <div className="search-li">
        {/* 检查状态:
        <Select
            defaultValue={0 }
            style={{
                width: 120,
            }}
            onChange={handleChange}
            options={[
                {
                value: 1,
                label: '正常',
                },
                {
                value: 0,
                label: '异常',
                }
            ]}
        /> */}
      </div>
      <div className="search-li">
        <Button type="primary">
          <SearchOutlined />
          搜索
        </Button>
      </div>
      <div className="search-li">
        <Button>
          <PlusOutlined />
          会议室申请
        </Button>
      </div>
    </div>
  )
}

export default Search
