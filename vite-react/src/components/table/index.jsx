import { useEffect, useState } from "react"
import { Button, Popconfirm, Table, ConfigProvider, Tag, Pagination } from "antd"
import "./style.less"
import { useNavigate } from "react-router-dom"
import { conferenceExamine, conference_del } from "@/api/modules/conference/index.jsx"

const Tablemodel = (props) => {
  // 路由跳转
  const navigate = new useNavigate()
  const [loading, setLoading] = useState(false)
  //列表项
  const columns = [
    {
      title: "会议室名称",
      dataIndex: "conference_name",
      key: "conference_name"
    },
    {
      title: "会议主题",
      dataIndex: "conference_theme",
      key: "conference_theme"
    },
    {
      title: "使用时间",
      dataIndex: "use_start_end_time",
      key: "use_start_end_time",
      width: 120,
      render: (_, { use_start_time, use_end_time }) => (
        <>
          <span>{use_start_time.slice(0, 10)}至</span>
          <br />
          <span>{use_end_time.slice(0, 10)}</span>
        </>
      )
    },
    {
      title: "使用类型",
      dataIndex: "use_type",
      key: "use_type",
      render: (_, { use_type }) => <span>{use_type ? use_type.type_name : "未获取到使用信息"}</span>
    },
    {
      title: "申请人",
      dataIndex: "apply_name",
      key: "apply_name",
      render: (_, { apply_name }) => <span>{apply_name}</span>
    },
    {
      title: "申请部门",
      dataIndex: "apply_department",
      key: "apply_department",
      render: (_, { apply_department }) => (
        <span>{apply_department ? apply_department.deparment_name : "未获取到使用信息"}</span>
      )
    },
    {
      title: "申请时间",
      dataIndex: "apply_time",
      key: "apply_time",
      width: 160,
      render: (_, { apply_time }) => <span>{remigrate(apply_time)}</span>
    },
    {
      title: "申请状态",
      dataIndex: "apply_status",
      key: "apply_status",
      width: 90,
      render: (_, { apply_status }) => (
        <>
          {apply_status === 1 ? (
            <Tag color="#87d068">预约成功</Tag>
          ) : (
            <Tag color="#f50">预约中</Tag>
          )}
        </>
      )
    },
    {
      title: "检查状态",
      dataIndex: "check_status",
      key: "check_status",
      width: 90,
      render: (_, { check_status }) => (
        <>{check_status === 1 ? <Tag color="#87d068">正常</Tag> : <Tag color="#f50">异常</Tag>}</>
      )
    },
    {
      title: "操作",
      dataIndex: "todo",
      key: "todo",
      fixed: "right",
      width: 160,
      render: (_, { _id }) => (
        <>
          <Button className="clk" onClick={() => proview(_id)}>
            详情
          </Button>
          <Popconfirm
            placement="left"
            title="是否确定删除"
            okText="确定"
            cancelText="取消"
            onConfirm={() => conferencedel(_id)}
          >
            <Button className="clk">撤销</Button>
          </Popconfirm>
          {/* 审批按钮部分 */}
          {examineState == 0 ? (
            <Popconfirm
              placement="left"
              title={"审批是否通过？"}
              okText="是"
              cancelText="否"
              onConfirm={() => examine(_id, 1)}
              onCancel={() => examine(_id, 0)}
            >
              <Button className="clk">审批</Button>
            </Popconfirm>
          ) : (
            <></>
          )}
        </>
      )
    }
  ]
  //详情预览
  const proview = (_id) => {
    navigate("/admin/conference/proview?id=" + _id)
  }
  //调用删除接口
  const conferencedel = async (_id) => {
    await conference_del({ _id: _id })
    props.getList()
  }

  //权限状态  从redux中获取
  const [examineState, setExamineState] = useState(null)

  //进行审批,调用审批接口
  const examine = async (_id, statu) => {
    await conferenceExamine({ _id, apply_status: statu })
    props.getList(1, 3, {})
  }

  //审核权限判断
  const examineCheck = () => {
    // 从缓存的用户信息中获取权限信息
    setExamineState(JSON.parse(localStorage.getItem("role"))[0].status)
  }

  //日期格式化
  function pad(num) {
    return num.toString().padStart(2, "0")
  }
  const remigrate = (dateUntreated) => {
    let date = new Date(dateUntreated)
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
    return `${year}-${pad(month)}-${pad(day)} ${pad(hour)}:${pad(minute)}:${pad(second)}`
  }

  //页面渲染前获取当前用户权限
  useEffect(() => {
    setLoading(true)
    let timer = setTimeout(() => {
      setLoading(false)
    }, 2000)
    examineCheck()
    return () => clearTimeout(timer)
  }, [])

  return (
    <div className="reservation-table">
      <Table
        // style={{ height: "335px" }}
        scroll={{ y: 300, x: 1500 }}
        loading={loading}
        // scroll={{ x: 1500 }}
        columns={columns}
        dataSource={props.data}
        pagination={false}
      />
      {props.pagination()}
    </div>
  )
}

export default Tablemodel
