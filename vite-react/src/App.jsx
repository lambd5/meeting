import { staticRouter, dynamicRoutes, anyRoute } from "@/router/router"
import Admin from "./Admin"
import "./style.less"

const App = () => {
  let list = staticRouter.concat(dynamicRoutes)
  return <Admin routes={{ list }}></Admin>
}

export default App
