import {
  getClass,
  getType,
  getCheck,
  addClass,
  getDropt,
  changeClass,
  delClass,
  exportView,
  exportFile
} from "./modules/class/index"
import { getLogin, getRegister } from "./modules/login/index"
import { getRouter, getAllRouter } from "./modules/auth-router/index"
import { roleList, roleCreate, roleEdit, roleDelete } from "./modules/role/index"
import { userList, userAdd, userEdit, userDel } from "./modules/user/index"
export {
  getClass,
  getRegister,
  getLogin,
  getRouter,
  getType,
  getCheck,
  getAllRouter,
  addClass,
  getDropt,
  roleList,
  roleCreate,
  roleEdit,
  roleDelete,
  userList,
  userAdd,
  userEdit,
  userDel,
  changeClass,
  delClass,
  exportView,
  exportFile
}
