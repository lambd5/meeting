import request from "utils/request"

export const getBookingList = (data) =>
  request({
    path: "/bookinglist",
    data,
    method: "get"
  }) //申请列表

export const conferenceget = (data) =>
  request({
    path: "/meeting/list",
    data,
    method: "post"
  }) //会议列表获取

export const Booklist = (data) =>
  request({
    path: "/meeting/booklist",
    data,
    method: "post"
  })
export const conferenceDetails = (data) =>
  request({
    path: "/meeting/details",
    data,
    method: "post"
  }) // 详情页数据获取

export const conferenceExamine = (data) =>
  request({
    path: "/meeting/update",
    data,
    method: "post"
  }) // 会议审批接口

export const conference_del = (data) =>
  request({
    path: "/meeting/delete",
    data,
    method: "post"
  }) //会议撤销
export const updateexamine = (data) =>
  request({
    path: "/meeting/update",
    data,
    method: "get"
  }) // 会议审批接口

export const usetype = (data) =>
  request({
    path: "/meeting/typeList",
    data,
    method: "get"
  }) // 使用类型接口

export const postAddbook = (data) =>
  request({
    path: "/meeting/create",
    data,
    method: "post"
  }) // 会议申请添加

export const getDeparment = (data) =>
  request({
    path: "/meeting/deparmentlist",
    data,
    method: "get"
  }) // 部门数据

// export const export_excel = (data) =>
//   request({
//     path: "/meeting/exportExcel",
//     data,
//     method: "get"
//   })
// 会议数据导出
// 会议教室

export const TypeClass = (data) =>
  request({
    path: "/meeting/exportExcel",
    data,
    method: "get"
  }) // 会议数据导出
