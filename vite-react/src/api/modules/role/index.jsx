import request from "utils/request"

export const roleList = (data) =>
  request({
    path: "/auth/roleList",
    data,
    method: "post"
  }) //角色列表
//

export const roleCreate = (data) =>
  request({
    path: "/auth/roleCreate",
    data,
    method: "post"
  }) //角色创建

export const roleEdit = (data) =>
  request({
    path: "/auth/roleEdit",
    data,
    method: "post"
  }) //角色修改

export const roleDelete = (data) =>
  request({
    path: "/auth/roleDel",
    data,
    method: "post"
  }) //角色删除
