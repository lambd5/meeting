// import service from '../../../utils/model'
import request from "utils/request"

export const getLogin = (data) =>
  request({
    path: "/auth/login",
    data,
    method: "post"
  }) //登录
export const getRegister = (data) =>
  request({
    path: "/auth/register",
    data,
    method: "post"
  }) // 注册
