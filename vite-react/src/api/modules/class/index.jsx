import request from "utils/request"
export const getClass = (data) => request({ path: "/classroom/list", method: "post", data }) //教室列表
export const getType = () => request({ path: "/classroom/typeList", method: "get" }) //类型接口
export const getCheck = (data) => request({ path: "/classroom/examine", method: "post", data }) // 审批接口
export const addClass = (data) => request({ path: "/classroom/create", data, method: "post" }) //教室添加接口
export const getDropt = () => request({ path: "/classroom/deparmentList", method: "get" }) //部门接口
export const changeClass = (data) => request({ path: "/classroom/update", method: "post", data }) //教室修改接口
export const delClass = (data) => request({ path: "/classroom/delete", method: "post", data }) //删除教室
export const exportView = () => request({ path: "/classroom/exportExcel", method: "" })
export const exportFile = () => request({ path: "/classroom/exportClassRoomExcel", method: "get" })
