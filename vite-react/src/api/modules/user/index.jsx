import request from "utils/request"

export const userList = (data) =>
    request({
        path: "/auth/userList",
        data,
        method: "post"
    })
export const userAdd = (data) =>
    request({
        path: "/auth/userAdd",
        data,
        method: "post"
    })

export const userEdit = (data) =>
    request({
        path: "/auth/userEdit",
        data,
        method: "post"
    })

export const userDel = (data) =>
    request({
        path: "/auth/userDel",
        data,
        method: "post"
    })
