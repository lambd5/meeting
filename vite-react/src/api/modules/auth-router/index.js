// import service from '../../../utils/model'
import request from "utils/request"

export const getRouter = (data) =>
  request({
    path: "/auth/diffRoleList",
    data,
    method: "post"
  }) //权限

export const getAllRouter = (data) =>
  request({
    path: "/auth/routerDetail",
    data,
    method: "post"
  }) //所有权限
