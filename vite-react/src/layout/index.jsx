import { useState } from "react"
import { Outlet, useNavigate } from "react-router-dom"

import { Layout, Menu } from "antd"
import { AppstoreOutlined, MailOutlined, SettingOutlined } from "@ant-design/icons"
const { Header, Sider, Content } = Layout
//导航栏
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type
  }
}
const items = [
  getItem("办公", "sub1", <MailOutlined />, [getItem("无", "/")]),
  getItem("预约", "sub2", <AppstoreOutlined />, [getItem("预约班级", "/booking")]),
  getItem("部门", "sub4", <SettingOutlined />, [getItem("无", "/")])
]
const rootSubmenuKeys = ["sub1", "sub2", "sub4"]
//布局
const headerStyle = {
  textAlign: "center",
  color: "#fff",
  height: 64,
  paddingInline: 50,
  lineHeight: "4px",
  backgroundColor: "#7dbcea"
}
const contentStyle = {
  height: "100%",
  textAlign: "center",
  color: "#fff",
  backgroundColor: "#108ee9"
}
const sideStyle = {
  textAlign: "center",
  lineHeight: "120px",
  color: "#fff",
  backgroundColor: "#ffffff"
}
// const footerStyle = {
//   textAlign: "center",
//   color: "#fff",
//   backgroundColor: "#7dbcea"
// }
const BasicLayout = () => {
  //路由重定向
  const Navigate = useNavigate()
  //导航栏
  const [openKeys, setOpenKeys] = useState(["sub1"])
  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1)
    if (latestOpenKey && rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys)
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : [])
    }
  }
  return (
    <div>
      <Layout>
        <Sider style={sideStyle}>
          <Menu
            mode="inline"
            openKeys={openKeys}
            onOpenChange={onOpenChange}
            style={{
              width: 200
            }}
            items={items}
            onClick={(item) => {
              Navigate(item.key) //路由导航重定向
            }}
          />
        </Sider>
        <Layout>
          <Header style={headerStyle}>Header</Header>
          <Content style={contentStyle}>
            <Outlet />
          </Content>
        </Layout>
      </Layout>
    </div>
  )
}

export default BasicLayout
