import { Layout, Menu, theme } from "antd"
import { useNavigate } from "react-router-dom"
import { UserOutlined } from "@ant-design/icons"
import "./style.less"
import Down from "../dropdown/index"
const { Header, Content, Footer, Sider } = Layout

const BasicLayout = (props) => {
  const Navigate = useNavigate()
  const { routers } = props

  const items = routers
    .filter((items) => items.status == true)
    .map((item) => ({
      key: item.path,
      icon: item.icon,
      label: item.name,
      value: item.path
    }))

  const {
    token: { colorBgContainer }
  } = theme.useToken()
  const onClick = (item) => {
    const { key } = item
    Navigate(key)
  }
  return (
    <Layout>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => {}}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type)
        }}
      >
        <div className="demo-logo-vertical">
          <h3>后台管理系统</h3>
        </div>

        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["4"]}
          items={items}
          onClick={onClick}
        />
      </Sider>
      <Layout style={{ position: "relative" }}>
        {/* style={{ padding: 0, background: colorBgContainer }} */}
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
            display: "flex",
            // position: "fixed",
            position: "sticky",
            top: 0,
            zIndex: 1,
            width: "100%"
          }}
        >
          <div
            style={{
              display: "flex",
              // position: "relative",
              // left: "1130px",
              // width: "180px",
              height: "60px",
              lineHeight: "60px",
              marginRight: "16px",
              marginLeft: "auto"
            }}
          >
            <UserOutlined className="basic_user" />
            <Down></Down>
          </div>
        </Header>
        {/* <Breadcrumb
          style={{
            margin: "16px 0px 0px 16px"
          }}
        >
          <Breadcrumb.Item>User</Breadcrumb.Item>
          <Breadcrumb.Item>Bill</Breadcrumb.Item>
        </Breadcrumb> */}
        <Content style={{ margin: "24px 16px 0", height: "100%" }}>
          {/* minHeight: 360 */}
          <div
            style={{ padding: 24, background: colorBgContainer, borderRadius: 9, height: "100%" }}
          >
            {/* <div style={{ padding: 24, background: colorBgContainer }}> */}
            {props.children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>Ant Design ©2023 Created by Ant UED</Footer>
      </Layout>
    </Layout>
  )
}

export default BasicLayout
