import { DownOutlined } from "@ant-design/icons"
import { Dropdown, Space, Button, Modal } from "antd"
import { useState } from "react"
import { useNavigate } from "react-router-dom"

function App() {
  const Navigate = useNavigate()
  let username = JSON.parse(localStorage.getItem("user")) || ""
  const [isModalOpen, setIsModalOpen] = useState(false)
  const showModal = () => {
    setIsModalOpen(true)
  }
  const handleOk = () => {
    setIsModalOpen(false)
    outlogin()
  }
  const handleCancel = () => {
    setIsModalOpen(false)
  }
  const items = [
    {
      key: "1",
      label: <span onClick={showModal}>退出登录</span>
    }
  ]

  // 退出登录
  const outlogin = () => {
    sessionStorage.clear()
    localStorage.clear()
    Navigate("/login")
  }
  return (
    <div>
      <Modal title="是否退出" open={isModalOpen} footer={false} onCancel={handleCancel}>
        <div style={{ height: "20px" }}>
          <Button onClick={handleOk} type="primary" style={{ float: "right", margin: "0 20px" }}>
            确定
          </Button>
          <Button onClick={handleCancel} style={{ float: "right" }}>
            取消
          </Button>
        </div>
      </Modal>
      <Dropdown
        menu={{
          items
        }}
      >
        {/* <a onClick={(e) => e.preventDefault()}> */}
        <Button style={{ border: "0" }}>
          <Space>
            {username.username ? username.username : ""}
            <DownOutlined />
          </Space>
        </Button>
        {/* </a> */}
      </Dropdown>
    </div>
  )
}

export default App
