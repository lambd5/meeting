let createError = require("http-errors");
let express = require("express");
let path = require("path");
let cookieParser = require("cookie-parser");
let logger = require("morgan");
let cors = require("cors");
let expressJWT = require("express-jwt");
let classRoom = require("./routes/classroom"); // 教室路由表
let meeting = require("./routes/meeting"); // 会议路由表
let authRouter = require("./routes/auth"); // 登录鉴权的路由表

let app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

// 跨域
app.use(cors());
//配置验证token的中间件 express-jwt
// app.use(expressJWT({
//   //   //解析口令 需要和加密的时候保持一致
//   secret: "secretKey",
//   // 加密方式 SHA256 加密方式在express-jwt 里面叫做 HS256
//   algorithms: ["HS256"]
// }).unless({
//   //不需要token认证的路由  注意：如果有一级路由 需要把路由写完整
//   path: ["/auth/register", "/auth/login", "/auth/routerDetail", '/meeting/list', '/classroom/list', { url: /^\/upload/, methods: ["GET"] }]
// }))


app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/classroom", classRoom);
app.use("/meeting", meeting);
app.use("/auth", authRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
