const mongoose = require('mongoose')
// 连接数据库
mongoose.connect('mongodb://zy:zy123@112.124.35.69:27017/meeting?authSource=admin')

// mongoose.connect('mongodb://127.0.0.1:27017/bookclass')
const conn = mongoose.connection

conn.on('open', () => {
  console.log('连接成功', 'http://localhost:3666')
})
conn.on('error', (err) => {
  console.log('连接失败' + err)
})
module.exports = mongoose
