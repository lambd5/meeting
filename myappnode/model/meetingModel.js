const mongoose = require("./db");
// 会议模块  信息字段管理
const conferenceSchema = new mongoose.Schema({
  conference_name: String, // 会议主题
  conference_theme: String, // 会议名称
  use_start_time: Date, // 开始使用时间
  use_end_time: Date, // 结束使用时间
  use_type: {
    // 使用类型
    type: mongoose.Types.ObjectId,
    ref: "meeting_type",
  },
  apply_name: String, // 申请人
  apply_department: {
    // 申请部门
    type: mongoose.Types.ObjectId,
    ref: "meeting_deparment",
  },
  apply_time: {
    // 申请时间（创建的时间）
    type: Date,
    default: Date.now(),
  },
  apply_status: {
    // 申请状态
    type: Number,
    default: 0,
  },
  check_status: {
    // 检查状态
    type: Number,
    default: 1,
  },
});
const conferenceModel = mongoose.model("conference", conferenceSchema);

// 会议模块  使用类型
const typeSchema = new mongoose.Schema({
  type_name: String, // 类型名称
});

const typeModel = mongoose.model("meeting_type", typeSchema);

//  会议模块 部门管理(deparment)
const deparmentSchema = new mongoose.Schema({
  deparment_name: String, // 类型名称
});
const deparmentModel = mongoose.model("meeting_deparment", deparmentSchema);

// 会议教室  使用教室
const typeClassSchema = new mongoose.Schema({
  type_Class: String, // 类型名称
});
const typeClassModel = mongoose.model("meeting_typeClass", typeClassSchema);

module.exports = {
  typeModel,
  deparmentModel,
  conferenceModel,
  typeClassModel,
};
