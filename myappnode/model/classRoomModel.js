const mongoose = require("./db");

// 教室模块      信息字段管理
const classroomSchema = new mongoose.Schema({
  class_name: String, // 教室名称
  apply_department: { // 所属院校，申请部门
    type: mongoose.Types.ObjectId,
    ref: "classRoom_deparment",
  },
  current_state: { // 可用状态  0 可用，1不可用
    type: Number,
    default: 0,
  },
  availabel_types: { // 使用类型
    type: mongoose.Types.ObjectId,
    ref: "classRoom_type",
  },
  create_time: {
    type: Date,
    default: Date.now,
  },
  update_time: {
    type: Date,
    default: Date.now,
  },
}
);
const classroomModel = mongoose.model("classroom", classroomSchema,);

// 教室模块   使用类型
const typeSchema = new mongoose.Schema(
  { type_name: String, }, // 类型名称
);
const typeModel = mongoose.model("classRoom_type", typeSchema);


//  教室模块 部门
const deparmentSchema = new mongoose.Schema(
  { deparment_name: String, }, // 类型名称
);
const deparmentModel = mongoose.model("classRoom_deparment", deparmentSchema,);



module.exports = {
  typeModel,
  deparmentModel,
  classroomModel,
};
