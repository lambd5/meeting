const mongoose = require("./db");

// 登录管理
const loginSchema = new mongoose.Schema({
  username: String,
  password: String,
  status: {
    type: Number,
    default: 1,
  },
  phone: Number,
  create_time: {
    type: Date,
    default: Date.now,
  },
  update_time: {
    type: Date,
    default: Date.now,
  },
  rid: {
    type: mongoose.Types.ObjectId,
    ref: "role",
    default: '655efac3c81d468a83c29f86'
  },
});
const loginModel = mongoose.model("login", loginSchema, "login");

// 角色管理
const roleSchema = new mongoose.Schema({
  name: String, // 角色名称
  status: {
    // 角色状态
    type: Number,
    default: 1,
  },
  state: {
    // 角色是否禁用
    type: Number,
    default: 0,
  },
  menu_id: {
    type: [mongoose.Types.ObjectId],
    ref: "routes",
  },
  create_time: {
    type: Date,
    default: Date.now,
  },
  update_time: {
    type: Date,
    default: Date.now,
  },
});
const roleModel = mongoose.model("role", roleSchema, "role");

// 路由列表
const routesSchema = new mongoose.Schema({
  name: { type: String, default: "" },
  path: { type: String, default: "" },
  element: { type: String, default: "" },
  level: { type: Number, default: 1 },
  requireAuth: { type: Boolean, default: true },
  status: { type: Boolean },
  pid: {
    type: mongoose.Types.ObjectId,
    ref: "routes",
  },
});
const routesModel = mongoose.model("routes", routesSchema, "routes");

module.exports = {
  roleModel,
  loginModel,
  routesModel,
};
